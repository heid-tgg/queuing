import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import waiting_time
import numpy

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

st = ["MessageId", "Title", "Participates", "Start_Date", "End_Date"]
app.layout = html.Div(children=[
    html.H1(children='Queuing', style={"color": "red"}),

    html.Div(id="average_serving_time_container"),
    html.Div(id="standard_deviation_serving_time_exp_container"),
    html.Div(id="average_arrival_time_container"),
    html.Div(id="average_waiting_time"),

    dcc.Slider(id="average_serving_time", min=0, max=100, step=0.5, value=10),
    dcc.Slider(id="standard_deviation_serving_time_exp", min=-6, max=3, step=0.1, value=1),
    dcc.Slider(id="average_arrival_time", min=0, max=100, step=0.5, value=11),

])


@app.callback(Output('average_waiting_time', 'children'),
              [Input('average_serving_time', 'value'),
               Input('standard_deviation_serving_time_exp', 'value'),
            Input('average_arrival_time', 'value')])
def update_output(average_serving_time, standard_deviation_serving_time_exp, average_arrival_time):
    return str(waiting_time.calculate(average_serving_time,
                                      numpy.power(10, standard_deviation_serving_time_exp),
                                      60 / average_arrival_time))


a = [{"id": "average_serving_time", "display_text": "Serving time"},
     {"id": "standard_deviation_serving_time_exp", "display_text": "standard deviation"},
     {"id": "average_arrival_time", "display_text": "average arrival time"},]

for i, item in enumerate(a):
    @app.callback(Output(item["id"] + '_container', 'children'),
                  [Input(item["id"], 'value')])
    def update_function(input_value, display_text=item["display_text"]):
        return display_text + ": " + str(input_value)

    a[i]["update_function"] = update_function

if __name__ == '__main__':
    app.run_server(debug=True)
