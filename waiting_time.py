"""
The time is given in minutes if not stated otherwise
"""
import numpy
from scipy import constants

def calculate(serving_mean_time_minutes, serving_time_scale, arrival_rate_per_hour):
    minutes_per_hour = constants.hour / constants.minute
    lifetime_in_minutes = 100000
    number_of_desks = 1

    arrival_rate_per_minute = arrival_rate_per_hour / minutes_per_hour

    desk_occupied_next = numpy.zeros(number_of_desks)

    total_waiting_time = 0
    waiting_clients = 0
    arrived_clients = 0
    total_waiting_attendees = 0

    for time_slot in range(lifetime_in_minutes):
        desk_occupied_next -= 1
        desk_occupied = numpy.round(desk_occupied_next) > 0
        new_attendees = numpy.random.poisson(arrival_rate_per_minute)
        arrived_clients += new_attendees
        waiting_clients += new_attendees

        for i, desk in enumerate(desk_occupied):
            if not desk and waiting_clients > 0:
                desk_occupied[i] = True
                desk_occupied_next[i] = numpy.random.normal(serving_mean_time_minutes, serving_time_scale)
                waiting_clients -= 1
        total_waiting_time += waiting_clients
        total_waiting_attendees += waiting_clients

    print("total waiting time:", total_waiting_time)
    print("average waiting time:", total_waiting_time / arrived_clients)
    print("not served attendees", waiting_clients)
    print("total attendees", arrived_clients)
    print("average of waiting attendees", total_waiting_attendees / lifetime_in_minutes)
    return total_waiting_time / arrived_clients
